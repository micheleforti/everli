<?php

declare(strict_types=1);

$path = new Path('/a/b/c/d');
$path->cd('../x');
echo $path->currentPath;

class Path
{
    public string $currentPath;

    public function __construct(string $path)
    {
        if ($this->isValidInitialAbsolutePath($path)) {
            $this->currentPath = $path;
        } else {
            throw new \Exception("Invalid path: $path");
        }
    }

    public function cd(string $newPath): void
    {
        if ($this->isValidPath($newPath)) {
            // Split the current and new paths into arrays of directories
            $currentDirs = explode("/", $this->currentPath);
            $newDirs = explode("/", $newPath);

            // Check if the new path starts with '/', meaning it is an absolute path
            if ($newPath[0] == "/") {
                // Reset the current path array to an empty array
                $currentDirs = [];
            }

            foreach ($newDirs as $dir) {
                // If the directory is '..', remove the last element from the current path array
                if ($dir == "..") {
                    \array_pop($currentDirs);
                }
                // Otherwise, append the directory to the current path array
                else {
                    \array_push($currentDirs, $dir);
                }
            }

            // Check if the current path array is empty, meaning the new path goes before the root
            if (empty($currentDirs)) {
                // Throw an exception
                throw new \Exception("Invalid path: $newPath");
            }
            // Otherwise, join the current path array with '/' and assign it to the currentPath property
            else {
                $this->currentPath = \implode("/", $currentDirs);
            }
        } else {
            throw new \Exception("Invalid path: $newPath");
        }
    }

    /**
     * Validate an initial absolute path (only English alphabet)
     */
    public function isValidInitialAbsolutePath(string $path): bool
    {
        $pattern = "/^\/[a-zA-Z]+(\/[a-zA-Z]+)*$/";
        return (bool)preg_match($pattern, $path);
    }

    /**
     * Validate a path change (only English alphabet)
     */
    public function isValidPath(string $path): bool
    {
        $pattern = "/^((\.\.)|[a-zA-Z]+)*(\/((\.\.)|[a-zA-Z]+))*$/";
        return (bool)preg_match($pattern, $path);
    }
}
