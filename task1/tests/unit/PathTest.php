<?php

declare(strict_types=1);

namespace tests\unit;

use Path;
use PHPUnit\Framework\TestCase;

require_once realpath(dirname(__FILE__)) . '/../../change_directory.php';

class PathTest extends TestCase
{
    private Path $path;

    public function setUp(): void
    {
        $this->path = new Path("/a/b/c/d");
    }

    /**
     * @dataProvider initialAbsolutePathsProvider
     */
    public function testIsValidInitialAbsolutePath(string $path, bool $expected): void
    {
        $valid = $this->path->isValidInitialAbsolutePath($path);
        $this->assertEquals($expected, $valid);
    }

    /**
     * @return array<string, array<string, bool>>
     */
    /** @phpstan-ignore-next-line */
    public static function initialAbsolutePathsProvider(): array
    {
        return [
            'Test absolute first level' => ['/a', true],
            'Test absolute second level' => ['/a/b', true],
            'Test absolute final slash' => ['/a/b/', false],
            'Test absolute numbers' => ['/1', false],
            'Test absolute empty string' => ['', false],
            'Test absolute initial slash' => ['a/b', false],
            'Test absolute double slash' => ['/a//b', false]
        ];
    }

    /**
     * @dataProvider pathsProvider
     */
    public function testIsValidPath(string $path, bool $expected): void
    {
        $valid = $this->path->isValidPath($path);
        $this->assertEquals($expected, $valid);
    }

    /**
     * @return array<string, array<string, bool>>
     */
    /** @phpstan-ignore-next-line */
    public static function pathsProvider(): array
    {
        return [
            'Test absolute first level' => ['/a', true],
            'Test absolute second level' => ['/a/b', true],
            'Test relative first level' => ['a', true],
            'Test relative second level' => ['a/b', true],
            'Test ..' => ['../a/b', true],
            'Test .. in the middle relative' => ['a/b/../c', true],
            'Test .. in the middle absolute' => ['/a/../c', true],
            'Test final slash' => ['a/b/', false],
            'Test numbers' => ['1', false],
        ];
    }

    /**
     * @dataProvider relativePathsProvider
     */
    public function testCdWithRelativePaths(string $path, string $expected): void
    {
        $this->path->cd($path);
        $this->assertEquals($expected, $this->path->currentPath);
    }

    /**
     * @return array<string, array<string, string>>
     */
    /** @phpstan-ignore-next-line */
    public static function relativePathsProvider(): array
    {
        return [
            'Test first level' => ['../x', '/a/b/c/x'],
            'Test second level' => ['../../x', '/a/b/x'],
            'Test third level' => ['../../../x', '/a/x'],
            'Test complex path' => ['../../x/../y/z', '/a/b/y/z'],
        ];
    }

    /**
     * @dataProvider absolutePathsProvider
     */
    public function testCdWithAbsolutePaths(string $path, string $expected): void
    {
        $this->path->cd($path);
        $this->assertEquals($expected, $this->path->currentPath);
    }

    /**
     * @return array<string, array<string, string>>
     */
    /** @phpstan-ignore-next-line */
    public static function absolutePathsProvider(): array
    {
        return [
            'Test first level' => ['/x', '/x'],
            'Test second level' => ['/x/y/../z', '/x/z'],
        ];
    }

    /**
     * @dataProvider invalidPathsProvider
     */
    public function testCdInvalidPaths(string $path, string $expected): void
    {
        // Try to change the directory to an invalid relative path and assert that an exception is thrown
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage($expected);
        $this->path->cd($path);
    }

    /**
     * @return array<string, array<string, string>>
     */
    /** @phpstan-ignore-next-line */
    public static function invalidPathsProvider(): array
    {
        return [
            'Invalid path that goes before the root' => ['../../../../..', 'Invalid path: ../../../../..'],
        ];
    }
}
