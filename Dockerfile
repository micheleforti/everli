FROM php:8.2-rc-fpm-alpine

COPY . /app

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer require --dev phpunit/phpunit

WORKDIR /app