<?php

declare(strict_types=1);

require 'vendor/autoload.php';

use RFHaversini\Distance;

try {
    // Read locations from json file
    $locationsFile = read_local_file('task2/data/locations.json');
    $locations = (array)\json_decode($locationsFile, true);

    // Read shoppers from json file
    $shoppersFile = read_local_file('task2/data/shoppers.json');
    $shoppers = (array)\json_decode($shoppersFile, true);

    print_r(calculate_coverage($locations, $shoppers));
} catch (Exception $e) {
    // TODO: manage and log errors
    die($e->getMessage());
}

/**
 * Compute shoppers coverage based on locations list
 *
 * @phpstan-ignore-next-line
 * ignore the error that locations and shoppers are a generic array,
 * a better solution would be to map them to defined classes
 */
function calculate_coverage(array $locations, array $shoppers): array
{
    $result = [];
    $locationsCount = count($locations);
    foreach ($shoppers as $shopper) {
        // if the shop is not enabled, continue to the next one
        if (!$shopper['enabled']) {
            continue;
        }

        // compute coverage
        $coverage = 0;
        foreach ($locations as $location) {
            $distance = Distance::toKilometers($shopper['lat'], $shopper['lng'], $location['lat'], $location['lng']);
            if ($distance < 10) {
                $coverage++;
            }
        }

        // add shopper to final result
        $result[] = [
            'shopper_id' => $shopper['id'],
            'coverage' => round($coverage / $locationsCount * 100)
        ];
    }

    // sort final result by coverage desc
    usort($result, function ($a, $b) {
        return $b['coverage'] - $a['coverage'];
    });

    return $result;
}

/**
 * Read file from local file system
 * @throws Exception
 */
function read_local_file(string $filePath): string
{
    $file = fopen($filePath, 'r');
    if (!$file) {
        throw new Exception("Unable to open file: $filePath");
    }

    $content = fread($file, (int)filesize($filePath));
    fclose($file);

    return (string)$content;
}
