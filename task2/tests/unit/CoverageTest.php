<?php

declare(strict_types=1);

namespace tests\unit;

use Path;
use PHPUnit\Framework\TestCase;

require_once realpath(dirname(__FILE__)) . '/../../haversine_coverage.php';

class CoverageTest extends TestCase
{
    public function testCalculateCoverage(): void
    {
        // Test data
        $locations = [
            ['lat' => 51.5074, 'lng' => -0.1278],
            ['lat' => 40.7128, 'lng' => -74.0060],
            ['lat' => 34.0522, 'lng' => -118.2437],
            // Add more locations as needed
        ];

        $shoppers = [
            ['id' => 1, 'lat' => 51.7520, 'lng' => -1.2577, 'enabled' => true],
            ['id' => 2, 'lat' => 40.4168, 'lng' => -3.7038, 'enabled' => true],
            ['id' => 3, 'lat' => 34.0522, 'lng' => -118.2437, 'enabled' => true],
            // Add more shoppers as needed
        ];

        // Expected result
        $expected = [
            [
                'shopper_id' => 3,
                'coverage' => 33,
            ],
            [
                'shopper_id' => 1,
                'coverage' => 0,
            ],
            [
                'shopper_id' => 2,
                'coverage' => 0,
            ],
            // Add expected results for each shopper
        ];

        // Call the function
        $result = calculate_coverage($locations, $shoppers);

        // Assert the result
        $this->assertEquals($expected, $result);
    }
}
