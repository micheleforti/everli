upd:
	docker compose up -d
upb:
	docker compose up -d --build --force-recreate
composer_install:
	docker exec -it everli-php composer i
task1_run:
	docker exec -it everli-php php task1/change_directory.php
task1_test:
	docker exec -it everli-php ./vendor/bin/phpunit task1/tests/unit
task2_run:
	docker exec -it everli-php php task2/haversine_coverage.php
task2_test:
	docker exec -it everli-php ./vendor/bin/phpunit task2/tests/unit
task1_cs_fixer:
	docker exec -it everli-php ./vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix task1
task2_cs_fixer:
	docker exec -it everli-php ./vendor/friendsofphp/php-cs-fixer/php-cs-fixer fix task2
task1_php_stan:
	docker exec -it everli-php ./vendor/bin/phpstan analyse task1 --level 9
task2_php_stan:
	docker exec -it everli-php ./vendor/bin/phpstan analyse task2 --level 9
