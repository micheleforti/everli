# EVERLI

There is a folder for each task. Inside each folder, you can find:
- the file to start the code
- a README with the task requirements
- the test/unit folder with some unit tests (surely the coverage of the various cases can be increased)

All commands are present in the Makefile.  
If you can't run commands from the Makefile, copy and paste them into the terminal.

## First installation or re-installation
- Create and start docker container
```
make upb
```
- Install package from Composer
```
make composer_install
```

## Commands
> All examples are for task1, for 2 use prefix task2

### Run projects
- Run Task1
```
make task1_run
```
- Run Task1 unit test
```
make task1_test
```

### Code linting
```
make task1_cs_fixer
```

### Static analysis
```
make task1_php_stan
```